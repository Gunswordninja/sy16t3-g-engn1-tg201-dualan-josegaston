#pragma once
#include "GL/glew.h"

class Vector3
{
public:
	Vector3();
	Vector3(GLfloat x, GLfloat y, GLfloat z);

	GLfloat x;
	GLfloat y;
	GLfloat z;
};

