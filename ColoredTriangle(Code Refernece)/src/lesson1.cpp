#include <iostream>
#include <sstream>

#define GLEW_STATIC //Needed for static linking GLEW (not needed for dynamic linked GLEW)
#include "GL/glew.h" // IMPORTANT: glew.h must be included BEFORE glfw3.h
// What GLEW does is it gets available function pointers from your GPU that are supported via the driver. You can do this manually but GLEW makes it easy
#include "GLFW/glfw3.h" // IMPORTAT: glfw3.h must be included AFTER glew.h

#include <iostream>
#include "Window.h"
#include "Mesh.h"
#include "Shader.h"
using namespace std;

const char* APP_TITLE = "Hello Triangle";
const int gWindowWidth = 800;
const int gWindowHeight = 600;
bool gFullscreen = false;

// To keep things simple, we write a shader instead
const GLchar* vertextShaderSrc =
"#version 330 core\n"
"layout (location = 0) in vec3 pos;"
"layout (location = 1) in vec3 color;"
"out vec3 vert_color;"
"void main()"
"{"
"	vert_color = color;"
"	gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);"
"}";

const GLchar* fragmentShaderSrc =
"#version 330 core\n"
"in vec3 vert_color;"
"out vec4 frag_color;"
"void main()"
"{"
"	frag_color = vec4(vert_color, 1.0f);"
"}";


int main()
{
	Window windowApp(3, 3);
	windowApp.setWindowWidth(800);
	windowApp.setWindowHeight(600);
	windowApp.setWindowTitle("Hello Clean Code");

	GLFWwindow *window = windowApp.init();
	if (window == NULL)
	{
		return -1;
	}
	windowApp.clear(0.23f, 0.38f, 0.47f, 1.0f);

	//GLfloat vertices[] = {
	//	0.0f, 0.5f, 0.0f, //top
	//	0.5f, -0.5f, 0.0f, // right
	//	-0.5f, -0.5f, 0.0f // left
	//};

	Vertex vertices[] = {
		Vertex(Vector3(0.0f, 0.5f, 0.0f), Color::Blue),
		Vertex(Vector3(0.5f, -0.5f, 0.0f), Color::Red),
		Vertex(Vector3(-0.5f, -0.5f, 0.0f), Color::Blue)
	};


	Mesh *triangle = new Mesh(vertices, 3);
	
	Shader vertextShader;
	vertextShader.loadFromString(vertextShaderSrc, Shader::Type::Vertex);
	GLuint vs = vertextShader.getShaderId();

	Shader fragmentShader;
	fragmentShader.loadFromString(fragmentShaderSrc, Shader::Type::Fragment);
	GLuint fs = fragmentShader.getShaderId();

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vs);
	glAttachShader(shaderProgram, fs);
	glLinkProgram(shaderProgram);

	GLint result;
	GLchar infoLog[512];
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shaderProgram, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Shader Program linker failure " << infoLog << endl;
	}

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Wireframe
	// Main application loop
	while (!windowApp.getShouldClose())
	{
		glfwPollEvents();
		glClear(GL_COLOR_BUFFER_BIT);
		
		glUseProgram(shaderProgram);
		triangle->draw();

		glfwSwapBuffers(window); // Double buffering
	}

	glDeleteProgram(shaderProgram);
	glfwTerminate(); // You have to call this right before you exit the program
	return 0;
}