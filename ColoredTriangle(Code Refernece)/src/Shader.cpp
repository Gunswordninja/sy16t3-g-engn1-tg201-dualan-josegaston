#include "Shader.h"
#include <iostream>


Shader::Shader()
{
	mShaderId = 0;
}


Shader::~Shader()
{
	glDeleteShader(mShaderId);
}

GLuint Shader::getShaderId()
{
	return mShaderId;
}

bool Shader::loadFromString(const GLchar * src, Type type)
{
	if (type == Vertex) 
		mShaderId = glCreateShader(GL_VERTEX_SHADER);
	else if (type == Fragment) 
		mShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(mShaderId, 1, &src, NULL);
	glCompileShader(mShaderId);

	GLint result;
	GLchar infoLog[512];
	glGetShaderiv(mShaderId, GL_COMPILE_STATUS, &result);

	if (!result)
	{
		glGetShaderInfoLog(mShaderId, sizeof(infoLog), NULL, infoLog);
		std::cout << "Error! Vertex shader failed to compile." << infoLog << std::endl;
		return false;
	}

	return true;
}
