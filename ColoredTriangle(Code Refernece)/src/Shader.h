#pragma once
#include "GL/glew.h"
class Shader
{
public:
	Shader();
	~Shader();

	enum Type { Vertex, Fragment };

	GLuint getShaderId();
	bool loadFromString(const GLchar* src, Type type);
private:
	GLuint mShaderId;
};

