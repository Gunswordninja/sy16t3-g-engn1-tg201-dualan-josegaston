#pragma once
#include "GL/glew.h"
class Color
{
public:
	Color();
	Color(GLfloat r, GLfloat g, GLfloat b);

	GLfloat r;
	GLfloat g;
	GLfloat b;

	static const Color Black;
	static const Color White;
	static const Color Red;
	static const Color Green;
	static const Color Blue;
	static const Color Yellow;
	static const Color Cyan;
	static const Color Magenta;
};

