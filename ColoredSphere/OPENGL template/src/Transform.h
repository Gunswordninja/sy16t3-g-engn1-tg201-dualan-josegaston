#pragma once
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtx\transform.hpp"

namespace MyGL {
	class Transform
	{
	public:
		Transform();
		Transform(glm::mat4 matrix);
		~Transform();

		void translate(glm::vec3 vec);
		void rotate(float angle, glm::vec3 axis);
		glm::mat4 & getMatrix();
	private:
		glm::mat4 mMatrix;
	};
}