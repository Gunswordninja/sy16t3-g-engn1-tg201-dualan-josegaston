#include "Window.h"

using namespace MyGL;

MyGL::Window::Window(int glMajorVersion, int glMinorVersion, int width, int height, char* title)
{
	mGlMajorVersion = glMajorVersion;
	mGlMinorVersion = glMinorVersion;
	mWindowWidth = width;
	mWindowHeight = height;
	mWindowTitle = title;
	mInitialized = false;

	if (!glfwInit()) // If GLFW initialize failed, w/c rarely happens. We want to exit out of the program if it happens
	{
		cerr << "GLFW initialization failed" << endl;
		return; // Return value of non 0 indicates an error
	}

	// Set opengl version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, mGlMajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, mGlMinorVersion);
	// Set profile (compatibility (old) or core (modern) )
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Set forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Get a pointer to a window. This is where we will render
	GLFWwindow *window = glfwCreateWindow(mWindowWidth, mWindowHeight, mWindowTitle, NULL, NULL);

	// If the window was null, something bad happened
	if (window == NULL)
	{
		cerr << "Failed to create GLFW window" << endl;
		glfwTerminate(); // You have to call this right before you exit the program
		return;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE; // GLEW documentation tells us to set this value to true if using modern OpenGL

	if (glewInit() != GLEW_OK)
	{
		cerr << "GLEW initialization failed" << endl;
		glfwTerminate(); // You have to call this right before you exit the program
		return; // Return value of non 0 indicates an error
	}

	mInitialized = true;
	mWindow = window;
}

MyGL::Window::~Window()
{
	glfwTerminate();
}

void MyGL::Window::init()
{
	
}

void MyGL::Window::setWindowWidth(int width)
{
	if (mInitialized) {
		cerr << "Cannot set window width, window already initialized";
		return;
	}
	mWindowWidth = width;
}

void MyGL::Window::setWindowHeight(int height)
{
	if (mInitialized) {
		cerr << "Cannot set window height, window already initialized";
		return;
	}
	mWindowHeight = height;
}

void MyGL::Window::setWindowTitle(char * title)
{
	mWindowTitle = title;
}

bool MyGL::Window::getKey(int key, int action)
{
	int state = glfwGetKey(mWindow, key);
	if (state == action)
		return true;
	return false;
}

void MyGL::Window::close()
{
	glfwSetWindowShouldClose(mWindow, true);
}

bool MyGL::Window::getShouldClose()
{
	return glfwWindowShouldClose(mWindow);
}

void MyGL::Window::update()
{
	glfwPollEvents();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MyGL::Window::present()
{
	glfwSwapBuffers(mWindow); // Double buffering
}

//// Parameters must be in order. Function name can be any
//void glfw_onKey(GLFWwindow* window, int key, int scancode, int action, int mode)
//{
//	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
//	{
//		glfwSetWindowShouldClose(window, true);
//	}
//}