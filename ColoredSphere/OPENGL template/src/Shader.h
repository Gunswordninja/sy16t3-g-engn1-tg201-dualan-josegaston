#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
#include <iostream>
using namespace std;
namespace MyGL {
	class Shader
	{
	public:
		Shader();
		~Shader();

		enum Type {Vertex, Fragment};

		GLuint getShaderId();
		bool loadFromString(const GLchar* src, Type type);
		
	private:
		GLuint mShaderId;
	};
}

