#pragma once
#define GLEW_STATIC
#include <iostream>
#include "GL/glew.h"
namespace MyGL {
	class Vector3
	{
	public:
		Vector3();
		Vector3(GLfloat x, GLfloat y, GLfloat z);
		void VectorPrint(int posNum, int triangleNum);
		~Vector3();
		
		GLfloat x;
		GLfloat y;
		GLfloat z;
	};
}