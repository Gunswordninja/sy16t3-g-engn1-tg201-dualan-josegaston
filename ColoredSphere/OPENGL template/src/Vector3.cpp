#include "Vector3.h"

using namespace MyGL;

Vector3::Vector3()
{
}

MyGL::Vector3::Vector3(GLfloat x, GLfloat y, GLfloat z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void MyGL::Vector3::VectorPrint(int posNum, int triangleNum)
{
	std::cout << " TriangleNum : " << triangleNum << " PositionNum : " << posNum << " x : " << x << " y : " << y << " z : " << z << std::endl;
}


Vector3::~Vector3()
{
}
