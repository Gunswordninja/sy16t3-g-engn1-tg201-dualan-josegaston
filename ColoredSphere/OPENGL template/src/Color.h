#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
namespace MyGL {
	class Color
	{
	public:
		Color();
		Color(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
		~Color();

		GLfloat r;
		GLfloat g;
		GLfloat b;
		GLfloat a;

		static const Color Black;
		static const Color Red;
		static const Color Green;
		static const Color Blue;
		static const Color White;
		static const Color Yellow;
		static const Color Cyan;
		static const Color Magenta;
	};
}