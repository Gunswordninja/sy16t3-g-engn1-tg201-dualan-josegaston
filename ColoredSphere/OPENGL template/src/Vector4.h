#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
namespace MyGL {
	struct Vector4
	{
		GLfloat x;
		GLfloat y;
		GLfloat z;
		GLfloat w;
	};
}