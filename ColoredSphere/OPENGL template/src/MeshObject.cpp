#include "MeshObject.h"
#include <iostream>
using namespace MyGL;


MyGL::MeshObject::MeshObject(Vertex vertices[], size_t vertexCount)
{
	if (vertexCount == 0) return;
	
	mSize = vertexCount;

	// Store all vertex information in seperate arrays
	GLfloat *positions = new GLfloat[vertexCount*3];
	unsigned int vectorCounter = 0;
	for (unsigned int i = 0; i < vertexCount; i++) {
		positions[vectorCounter] = vertices[i].position.x;
		positions[vectorCounter+1] = vertices[i].position.y;
		positions[vectorCounter+2] = vertices[i].position.z;
		vectorCounter = 3 * (i + 1);
	}

	GLfloat *colors = new GLfloat[vertexCount * 3];
	unsigned int colorCounter = 0;
	for (unsigned int i = 0; i < vertexCount; i++) {
		colors[colorCounter] = vertices[i].color.r;
		colors[colorCounter + 1] = vertices[i].color.g;
		colors[colorCounter + 2] = vertices[i].color.b;
		//positions[colorCounter + 3] = vertices[i].color.a;
		//colorCounter = 4 * (i + 1);
		colorCounter = 3 * (i + 1);
	}

	// Construct buffers
	glGenBuffers(1, &mVboPos);
	glBindBuffer(GL_ARRAY_BUFFER, mVboPos);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexCount * 3, positions, GL_STATIC_DRAW);

	glGenBuffers(1, &mVboColor);
	glBindBuffer(GL_ARRAY_BUFFER, mVboColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexCount * 3, colors, GL_STATIC_DRAW);
	
	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);

	// Must activate buffer to know what we're describing
	glBindBuffer(GL_ARRAY_BUFFER, mVboPos);
	// Attributes for position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	// Must activate buffer to know what we're describing
	glBindBuffer(GL_ARRAY_BUFFER, mVboColor);
	// Attributes for color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	delete[] positions;
	delete[] colors;
}

MyGL::MeshObject::MeshObject(int stacks, int slices, float radius)
{
}

MeshObject::~MeshObject()
{
	glDeleteVertexArrays(1, &mVao);
	glDeleteBuffers(1, &mVboPos);
	glDeleteBuffers(1, &mVboColor);
}

void MyGL::MeshObject::draw()
{
	glBindVertexArray(mVao);
	glDrawArrays(GL_TRIANGLES, 0, mSize);
	glBindVertexArray(0);
}