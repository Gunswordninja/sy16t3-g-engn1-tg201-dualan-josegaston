#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
#include "Drawable.h"
#include "Vertex.h"

// For performance considerations on setting up buffers
// See: https://www.opengl.org/wiki/Vertex_Specification_Best_Practices

namespace MyGL {
	class MeshObject : public Drawable
	{
	public:
		MeshObject(Vertex vertices[], size_t vertexCount);
		MeshObject(int stacks, int slices, float radius);
		~MeshObject();

		void draw();
	private:
		GLuint mVboPos;
		GLuint mVboColor;
		GLuint mVao;
		size_t mSize;
	};
}