#include "RenderTarget.h"

MyGL::RenderTarget::RenderTarget()
{
	mShaderProgramId = glCreateProgram();
}

MyGL::RenderTarget::~RenderTarget()
{
	if (mVertexShader != NULL) delete mVertexShader;
	if (mFragmentShader != NULL) delete mFragmentShader;

	glDeleteProgram(mShaderProgramId);
}

void MyGL::RenderTarget::setVertexShader(const GLchar * src)
{
	if (mVertexShader != NULL)
	{
		glDetachShader(mShaderProgramId, mVertexShader->getShaderId());
		delete mVertexShader;
	}
	mVertexShader = new Shader();
	mVertexShader->loadFromString(src, Shader::Type::Vertex);
}

void MyGL::RenderTarget::setFragmentShader(const GLchar * src)
{
	if (mFragmentShader != NULL)
	{
		glDetachShader(mShaderProgramId, mFragmentShader->getShaderId());
		delete mFragmentShader;
	}
	mFragmentShader = new Shader();
	mFragmentShader->loadFromString(src, Shader::Type::Fragment);
}

void MyGL::RenderTarget::compileShaders()
{
	if (mVertexShader != NULL)
	{
		glAttachShader(mShaderProgramId, mVertexShader->getShaderId());
	}
	
	if (mFragmentShader != NULL)
	{
		glAttachShader(mShaderProgramId, mFragmentShader->getShaderId());
	}

	glLinkProgram(mShaderProgramId);

	GLint result;
	GLchar infoLog[512];
	glGetProgramiv(mShaderProgramId, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(mShaderProgramId, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Shader Program linker failure " << infoLog << endl;
	}

	delete mVertexShader;
	mVertexShader = NULL;
	delete mFragmentShader;
	mFragmentShader = NULL;
}

void MyGL::RenderTarget::setWireframeMode(bool mode)
{
	glPolygonMode(GL_FRONT_AND_BACK, (mode ? GL_LINE : GL_FILL)); // Wireframe
}

void MyGL::RenderTarget::useZDepth(bool mode)
{
	mUserZBuffer = mode;
	if (mUserZBuffer) {
		// Enable depth test
		glEnable(GL_DEPTH_TEST);
		// Accept fragment if it closer to the camera than the former one
		glDepthFunc(GL_LESS);
	}
	else {
		glDisable(GL_DEPTH_TEST);
		glDepthFunc(GL_NEVER);
	}
}

void MyGL::RenderTarget::useBackfaceCulling(bool mode)
{
	mCullBackface = mode;
	if (mCullBackface) {
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	else {
		glDisable(GL_CULL_FACE);
	}
}

GLuint MyGL::RenderTarget::getShaderProgramId()
{
	return mShaderProgramId;
}

void MyGL::RenderTarget::clear(const Color & color)
{
	glClearColor(color.r, color.g, color.b, color.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MyGL::RenderTarget::draw(Drawable & drawable, GLuint matrixId, glm::mat4 & mvp)
{
	glUseProgram(mShaderProgramId);
	glm::mat4 matrix = mvp * drawable.getTransform().getMatrix();
	glUniformMatrix4fv(matrixId, 1, GL_FALSE, &matrix[0][0]);
	drawable.draw();
}
