#include <iostream>
#include <random>
#include <time.h>
#include "Node.h"

using namespace std;

void ConstructNodes(Node* &First, int numOfPlayers)
{
	Node* CurNode = new Node;
	CurNode = First;
	
	for (int i = 0; i < numOfPlayers; i++)
	{
		cout << endl << "Enter new player Name : ";
		string* newName = new string;
		cin >> *newName;
		CurNode->name = *newName;
		delete newName;
		if (i < numOfPlayers - 1)
		{
			Node* NewNode = new Node;
			CurNode->next = NewNode;
			CurNode->next->previous = CurNode;
			CurNode = CurNode->next;
		}
	}
	CurNode->next = First;
	First->previous = CurNode;
}

void RandomizeNode(Node* &CurNode, int Number)
{
	for (int i = 0; i < Number; i++)
	{
		CurNode = CurNode->next;
	}
}
void PlayRounds(int currentPlayers, Node *&currentNode)
{
	int RoundNumber = 1;

	while (currentPlayers != 1)
	{
		//Creating the number how how many turns per round
		cout << "ROUND " << RoundNumber << endl;
		cout << "=============================" << endl << endl;
		int nodeToEliminate = rand() % currentPlayers + 1;
		cout << "The number is " << nodeToEliminate << endl;
		cout << "Current Player : " << currentNode->name << endl;

		while (currentNode != NULL)
		{
			//Cycling through the players
			currentNode = currentNode->next;
			cout << nodeToEliminate << ". " << currentNode->name << endl;

			//Breaks loop when it number drops to 0
			if (nodeToEliminate <= 1)
			{
				break;
			}
			nodeToEliminate--;
		}

		cout << "The one eliminated is " << currentNode->name << endl;
		RoundNumber++;
		//Reassinging the adjacent Nodes Next and Previous Pointers
		currentNode->next->previous = currentNode->previous;
		currentNode->previous->next = currentNode->next;
		//Setting the current Node to a new node and deleting the previous one
		cout << "The next one starting is " << currentNode->next->name << endl;
		Node *nodeToDelete = currentNode;
		nodeToDelete = currentNode;
		currentNode = currentNode->next;
		delete nodeToDelete;

		system("pause");
		system("CLS");
		currentPlayers--;
	}
}


int main()
{
	srand(time(NULL));
	int *NumOfPlayers = new int;
	*NumOfPlayers = 0;

	cout << "Enter Number of Players" << endl;
	cin >> *NumOfPlayers;
	//First node of the Linked List
	Node *First = new Node;

	//Creating the Players
	ConstructNodes(First, *NumOfPlayers);
	
	//Setting the current Node and randomizing it
	Node *currentNode = First;
	RandomizeNode(currentNode, rand() % (*NumOfPlayers));
	cout << endl << "Current Player : " << currentNode->name << endl << endl;
	system("pause");
	system("CLS");

	//Play ing the game rounds
	PlayRounds(*NumOfPlayers, currentNode);

	//Announcing the winner
	cout << endl << endl <<"Congratulations " << currentNode->name << "! You are the last one standing." << endl;
	system("pause");
	return 0;
}


//Test Functions (working)

/*

//cout << "test next" << endl;
while (currentNode != NULL)
{

i++;
cout << currentNode->name << endl;
currentNode = currentNode->next;

if (i > *NumOfPlayers)
{
break;
}
}

cout << "test previous" << endl;
int x = 0;
while (currentNode != NULL)
{
x++;
cout << currentNode->name << endl;
currentNode = currentNode->previous;
if (x > *NumOfPlayers)
{
break;
}
}

void CreatePlayers(int NumOfPlayers, Node **&PlayerList)
{
//Constructing the Nodes
for (int i = 0; i < NumOfPlayers; i++)
{
cout << endl << "Enter new player Name : ";
string* newName = new string;
cin >> *newName;
Node* player = new Node;
player->name = *newName;
delete newName;
PlayerList[i] = player;
}

//Assigning Next and Previous to each Node
for (int i = 0; i < NumOfPlayers; i++)
{
PlayerList[i]->next = PlayerList[i + 1];
PlayerList[i]->previous = PlayerList[i - 1];

//Index Condition of the last
	if (i == NumOfPlayers - 1)
	{
	PlayerList[i]->next = PlayerList[0];
	}
//Index Condition of first
	if (i == 0)
	{
	PlayerList[i]->previous = PlayerList[(NumOfPlayers - 1)];
	}
}
}

//Initializing the Player list constructor
//Node ** PlayerList = new Node*[*NumOfPlayers];
/*Node *currentNode = NULL;//PlayerList[rand() % (*NumOfPlayers - 1)];
//delete[] PlayerList;

*/