#include "Vertex.h"

using namespace MyGL;

Vertex::Vertex()
{
	this->color = Color::White;
}

MyGL::Vertex::Vertex(const Vector3 & position)
{
	this->position = position;
	this->color = Color::White;
}

MyGL::Vertex::Vertex(const Vector3 & position, const Color & color)
{
	this->position = position;
	this->color = color;
}
