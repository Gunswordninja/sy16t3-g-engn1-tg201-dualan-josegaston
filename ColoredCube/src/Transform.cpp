#include "Transform.h"

using namespace MyGL;

Transform::Transform()
{
	mMatrix = glm::mat4(1.0f);
}

MyGL::Transform::Transform(glm::mat4 matrix)
{
	mMatrix = matrix;
}


Transform::~Transform()
{
}

void MyGL::Transform::translate(glm::vec3 vec)
{
	mMatrix = glm::translate(mMatrix, vec);
}

void MyGL::Transform::rotate(float angle, glm::vec3 axis)
{
	mMatrix = glm::rotate(mMatrix, angle, axis);
}

glm::mat4 & MyGL::Transform::getMatrix()
{
	return mMatrix;
}
