#pragma once
#include "Transform.h"
namespace MyGL {
	class Drawable
	{
	public:
		Drawable();
		virtual ~Drawable();

		virtual void draw();
		Transform & getTransform();
	protected:
		Transform mTransform;
	};
}