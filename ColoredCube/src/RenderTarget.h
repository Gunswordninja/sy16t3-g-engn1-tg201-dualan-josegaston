#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
#include "Color.h"
#include "Drawable.h"
#include "Shader.h"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
namespace MyGL {
	class RenderTarget
	{
	public:
		RenderTarget();
		~RenderTarget();
		void setVertexShader(const GLchar * src);
		void setFragmentShader(const GLchar * src);
		void compileShaders();
		void setWireframeMode(bool mode);
		void useZDepth(bool mode);
		void useBackfaceCulling(bool mode);
		GLuint getShaderProgramId();

		void clear(const Color &color = Color(0, 0, 0, 255));
		void draw(Drawable &drawable, GLuint matrixId, glm::mat4 & mvp);
	private:
		Shader *mVertexShader;
		Shader *mFragmentShader;
		GLuint mShaderProgramId;
		bool mUserZBuffer;
		bool mCullBackface;
	};
}