#pragma once
#include "Vector3.h"
#include "Color.h"

namespace MyGL {
	class Vertex
	{
	public:
		Vertex();
		Vertex(const Vector3 &position);
		Vertex(const Vector3 &position, const Color &color);
		
		Vector3 position;
		Color color;
	};
}
