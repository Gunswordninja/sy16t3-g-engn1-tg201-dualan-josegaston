#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
namespace MyGL {
	class Vector3
	{
	public:
		Vector3();
		Vector3(GLfloat x, GLfloat y, GLfloat z);
		~Vector3();
		
		GLfloat x;
		GLfloat y;
		GLfloat z;
	};
}