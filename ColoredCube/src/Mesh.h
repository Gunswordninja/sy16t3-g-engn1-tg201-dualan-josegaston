#pragma once
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "Drawable.h"
namespace MyGL {
	class Mesh : public Drawable
	{
	public:
		Mesh();
		~Mesh();

		void setVertices(GLfloat *vertices, int size);
		GLuint getVboId();
		GLuint getVaoId();
	private:
		GLfloat *mVertices;
		int mSize;
		GLuint mVboId;
		GLuint mVaoId;
	};
}