#pragma once
#define GLEW_STATIC
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <iostream>
using namespace std;
namespace MyGL {
	class Window
	{
	public:
		Window(int glMajorVersion, int glMinorVersion, int width, int height, char* title);
		~Window();
		
		void init();
		void setWindowWidth(int width);
		void setWindowHeight(int height);
		void setWindowTitle(char* title);
		bool getKey(int key, int action);
		void close();
		bool getShouldClose();
		void update();
		void present();
	private:
		GLFWwindow* mWindow;
		int mGlMajorVersion;
		int mGlMinorVersion;
		int mWindowWidth;
		int mWindowHeight;
		char* mWindowTitle;
		bool mInitialized;
	};
}