#include <iostream>
#include <sstream>

#define GLEW_STATIC //Needed for static linking GLEW (not needed for dynamic linked GLEW)
#include "GL/glew.h" // IMPORTANT: glew.h must be included BEFORE glfw3.h
// What GLEW does is it gets available function pointers from your GPU that are supported via the driver. You can do this manually but GLEW makes it easy
#include "GLFW/glfw3.h" // IMPORTAT: glfw3.h must be included AFTER glew.h

// Our includes
#include "Window.h"
#include "Shader.h"
#include "MeshObject.h"
#include "RenderTarget.h"
#include "Vector4.h"

// Include GLM
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtx\transform.hpp"
using namespace glm;

using namespace MyGL;
using namespace std;

// To keep things simple, we write a shader instead
const GLchar* vertextShaderSrc =
"#version 330 core\n"
"layout (location = 0) in vec3 pos;"
"layout (location = 1) in vec3 color;"
"out vec3 vert_color;"
"uniform mat4 MVP;"
"void main()"
"{"
"	vert_color = color;"
"	gl_Position = MVP * vec4(pos.x, pos.y, pos.z, 1.0);"
"}";

const GLchar* fragmentShaderSrc =
"#version 330 core\n"
"in vec3 vert_color;"
"out vec4 frag_color;"
"void main()"
"{"
"	frag_color = vec4(vert_color, 0.5f);"
"}";

MeshObject* createCube()
{
	// Triangle
	//Vertex vertices[] = {
	//	Vertex(Vector3(0.0f, 0.5f, 0.0f), Color::Red), // Top
	//	Vertex(Vector3(0.5f, -0.5f, 0.0f), Color::Green), // Right
	//	Vertex(Vector3(-0.5f, -0.5f, 0.0f), Color::Blue), // Left
	//};
	Vertex vertices[] = {
		Vertex(Vector3(-1.0f,-1.0f,-1.0f), Color::Red),
		Vertex(Vector3(-1.0f,-1.0f, 1.0f), Color::Red),
		Vertex(Vector3(-1.0f, 1.0f, 1.0f), Color::Red),

		Vertex(Vector3(1.0f, 1.0f,-1.0f), Color::Red),
		Vertex(Vector3(-1.0f,-1.0f,-1.0f), Color::Red),
		Vertex(Vector3(-1.0f, 1.0f,-1.0f), Color::Red),

		Vertex(Vector3(1.0f,-1.0f, 1.0f), Color::Blue),
		Vertex(Vector3(-1.0f,-1.0f,-1.0f), Color::Blue),
		Vertex(Vector3(1.0f,-1.0f,-1.0f), Color::Blue),

		Vertex(Vector3(1.0f, 1.0f,-1.0f), Color::Blue),
		Vertex(Vector3(1.0f,-1.0f,-1.0f), Color::Blue),
		Vertex(Vector3(-1.0f,-1.0f,-1.0f), Color::Blue),

		Vertex(Vector3(-1.0f,-1.0f,-1.0f), Color::Green),
		Vertex(Vector3(-1.0f, 1.0f, 1.0f), Color::Green),
		Vertex(Vector3(-1.0f, 1.0f,-1.0f), Color::Green),

		Vertex(Vector3(1.0f,-1.0f, 1.0f), Color::Green),
		Vertex(Vector3(-1.0f,-1.0f, 1.0f), Color::Green),
		Vertex(Vector3(-1.0f,-1.0f,-1.0f), Color::Green),

		Vertex(Vector3(-1.0f, 1.0f, 1.0f), Color::Yellow),
		Vertex(Vector3(-1.0f,-1.0f, 1.0f), Color::Yellow),
		Vertex(Vector3(1.0f,-1.0f, 1.0f), Color::Yellow),

		Vertex(Vector3(1.0f, 1.0f, 1.0f), Color::Yellow),
		Vertex(Vector3(1.0f,-1.0f,-1.0f), Color::Yellow),
		Vertex(Vector3(1.0f, 1.0f,-1.0f), Color::Yellow),

		Vertex(Vector3(1.0f,-1.0f,-1.0f), Color::Magenta),
		Vertex(Vector3(1.0f, 1.0f, 1.0f), Color::Magenta),
		Vertex(Vector3(1.0f,-1.0f, 1.0f), Color::Magenta),

		Vertex(Vector3(1.0f, 1.0f, 1.0f), Color::Magenta),
		Vertex(Vector3(1.0f, 1.0f,-1.0f), Color::Magenta),
		Vertex(Vector3(-1.0f, 1.0f,-1.0f), Color::Magenta),

		Vertex(Vector3(1.0f, 1.0f, 1.0f), Color::Cyan),
		Vertex(Vector3(-1.0f, 1.0f,-1.0f), Color::Cyan),
		Vertex(Vector3(-1.0f, 1.0f, 1.0f), Color::Cyan),

		Vertex(Vector3(1.0f, 1.0f, 1.0f), Color::Cyan),
		Vertex(Vector3(-1.0f, 1.0f, 1.0f), Color::Cyan),
		Vertex(Vector3(1.0f,-1.0f, 1.0f), Color::Cyan),
	};

	MeshObject *cube = new MeshObject(vertices, 12 * 3);
	return cube;
}

int main()
{
	Window windowApp(3, 3, 800, 600, "Hello Clean Code");

	// 3D Cube
	
	MeshObject *cube = createCube();
	cube->getTransform().translate(glm::vec3(0, 0, -5));
	cube->getTransform().rotate(45.0f, glm::vec3(0, 1, 0)); // Rotate by Y
	cube->getTransform().rotate(45.0f, glm::vec3(0, 0, 1)); // Rotate by Z

	RenderTarget renderTarget;
	renderTarget.setVertexShader(vertextShaderSrc);
	renderTarget.setFragmentShader(fragmentShaderSrc);
	renderTarget.compileShaders();
	renderTarget.setWireframeMode(true);
	renderTarget.useZDepth(true);
	renderTarget.useBackfaceCulling(true);

	///////// TODO : TRANSFER TO CAMERA CLASS /////////
	// Math code
	glm::mat4 projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f); // Set Field of View
	glm::mat4 view = glm::lookAt(
		glm::vec3(0, 0, -10), // Camera is at (0,0,-10), in World Space
		glm::vec3(0, 0, 0), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 MVP = projection * view * model;

	///////// TODO : TRANSFER TO RENDERTARGET CLASS /////////
	GLuint MatrixID = glGetUniformLocation(renderTarget.getShaderProgramId(), "MVP");
	///////// END TODO /////////

	// Main application loop
	while (!windowApp.getShouldClose())
	{
		//showFPS(window);
		windowApp.update(); // Query the window for keyboard or mouse events

		renderTarget.clear(Color(0.23f, 0.38f, 0.47f, 1.0f));

		// TODO: Upgrade draw code to accept Camera instead of MVP
		renderTarget.draw(*cube, MatrixID, MVP);

		windowApp.present();
	}

	delete cube;
	return 0;
}