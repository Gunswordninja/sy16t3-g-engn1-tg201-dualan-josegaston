#pragma once

#ifndef ENGINECONSTURCTION_H
#define ENGINECONSTRUCTION_H

#include <iostream>
#include <sstream>

#define GLEW_STATIC ///Needed for glew32s.lib
#include "GL/glew.h"
#include "GLFW/glfw3.h" // GLFW must come after Glew

using namespace std;

void onKeyPressed(GLFWwindow* window, int key, int scanCode, int action, int mode);
void onCursorUpdated(GLFWwindow* window, double x, double y);
void onClick(GLFWwindow* window, int button, int action, int mode);

const GLchar* vertexShaderSrc =
"#version 330 core\n"
"layout (location=0) in vec4 pos;"
"void main()"
"{"
"	gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);"
"}";

const GLchar* fragmentShaderSrc =
"#version 330 core\n"
"out vec4 frag_color;"
"void main()"
"{"
"	frag_color = vec4(.35f, .96f, .3f, 1.0);"
"}";

int Initialize(GLFWwindow* &window)
{
	if (!glfwInit())
	{
		cerr << "GLFW failed to initialize" << endl;
		return -1;
	}

	//sets opengl version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//Set openGl to modern Open GL
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//set forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	const int WINDOW_HEIGHT = 800;
	const int WINDOW_LENGTH = 600;
	const string APP_NAME = "Tits Mcgee";

	bool isFullScreen = true;
	
	if (isFullScreen)
	{
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* vidMode = glfwGetVideoMode(monitor);

		if (vidMode != NULL)
		{
			window = glfwCreateWindow(vidMode->width, vidMode->height, "dirty dan", NULL, NULL);
		}
	}

	else
	{
		window = glfwCreateWindow(WINDOW_LENGTH, WINDOW_HEIGHT, "TitsMcgee", NULL, NULL);
	}


	if (window == NULL)
	{
		cerr << "GLFWwindow failed to initialize" << endl;
		glfwTerminate();
		return -1;
	}

	//set window as current context
	glfwMakeContextCurrent(window);

	//input callback
	glfwSetKeyCallback(window, onKeyPressed);
	glfwSetMouseButtonCallback(window, onClick);
	glfwSetCursorPosCallback(window, onCursorUpdated);

	glewExperimental = GL_TRUE; //IMPORTANT DONT FORGET
	if (glewInit() != GLEW_OK)
	{
		cerr << "Glew initialization failed" << endl;
	}
}

void BufferCode(GLfloat vertices[], GLuint &vbo, GLuint &vao, GLint  &shaderProgram)
{
	//Buffer code

	//Ask Sir about this code, All values and size of the array do not go throught the parameter
	GLfloat verticess[] = {
		-1.0f,-1.0f,-1.0f, // triangle 1 : begin
		-1.0f,-1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f, // triangle 1 : end
		1.0f, 1.0f,-1.0f, // triangle 2 : begin
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f, // triangle 2 : end
		1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		1.0f,-1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f,-1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f,-1.0f, 1.0f
	};

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticess), verticess, GL_STATIC_DRAW);

	//vertex array object
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Turn on Vertex Array
	glEnableVertexAttribArray(0);

	/////////////SHADERSSSSS//////////////
	GLint result;
	GLchar infoLog[512];

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);//type of shhader
	glShaderSource(vs, 1, &vertexShaderSrc, NULL);//copy paste shader code
												  //into openGL
	glCompileShader(vs); //Compile the shader

	glGetShaderiv(vs, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(vs, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Fragment failed to compile." << infoLog << endl;
	}

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragmentShaderSrc, NULL);
	glCompileShader(fs);
	if (!result)
	{
		glGetShaderInfoLog(fs, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Fragment failed to compile." << infoLog << endl;
	}


	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vs);
	glAttachShader(shaderProgram, fs);
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shaderProgram, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Shader Program Linker failure" << infoLog << endl;
	}

	glDeleteShader(vs);
	glDeleteShader(fs);
}

void onKeyPressed(GLFWwindow* window, int key, int scanCode, int action, int mode)
{
	//check if esc key was pressed
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		//close the window
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

}

void onCursorUpdated(GLFWwindow * window, double x, double y)
{
	cout << "Cursor : (" << x << " , " << y << ")" << endl;
}

void onClick(GLFWwindow * window, int button, int action, int mode)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		cout << "Click Click" << endl;
	}
}

#endif //NODE_H#pragma once