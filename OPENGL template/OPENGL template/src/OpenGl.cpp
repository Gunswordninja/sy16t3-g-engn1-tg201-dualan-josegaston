
#include "HeaderFiles/EngineConstruction.h"


using namespace std;

//Simulation
void gameUpdate(GLFWwindow* window, double dt);
void renderUpdate(GLFWwindow* window, double dt, GLuint shaderProgam, GLuint vao);
void physicsUpdate(GLFWwindow* window, double dt);

void showFPS(GLFWwindow* window, double dt);

int main()
{
	GLFWwindow* window = NULL;
	
	Initialize(window);

	GLfloat vertices[] = {
		0.0f, 0.5f, 0.0f, // top
		0.5f, -0.5f, 0.0f, //right
		-0.5f, -0.5f, 0.0f //left
	};

	GLuint vbo = 0; //vertex buffer object
	GLuint vao = 0; // Vertex array object
	GLint shaderProgram = 0;

	BufferCode(vertices, vbo, vao, shaderProgram);

	glClearColor(.23f, .38f, .47f, 1.0f);

	double prevTime = 0;

	

	while (!glfwWindowShouldClose(window))
	{
		//compute Delta time
		double currentTime = glfwGetTime();
		double deltaTime = currentTime - prevTime;
		prevTime = currentTime;

		//1. Physics

		//2. Input
		glfwPollEvents();

		//3. Game Loop
		gameUpdate(window, deltaTime);

		//4. Render loop
		
		glClear(GL_COLOR_BUFFER_BIT);
		renderUpdate(window, deltaTime, shaderProgram, vao);
		glfwSwapBuffers(window);
		// Main application, game loop

	}

	cin.sync();
	cin.ignore();
	glfwTerminate();
	system("pause");
	return 0;
}



void gameUpdate(GLFWwindow * window, double dt)
{
	cout << dt <<  endl;
}

void renderUpdate(GLFWwindow * window, double dt, GLuint shaderProgam, GLuint vao)
{
	glUseProgram(shaderProgam);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 12*3);
	//glDrawArrays(GL_LINE_LOOP, 0, 4);
	//glMatrixMode(GL_PROJECTION);
	glBindVertexArray(0);
}

void physicsUpdate(GLFWwindow * window, double dt)
{
}

void showFPS(GLFWwindow * window, double dt)
{
	//double
}
