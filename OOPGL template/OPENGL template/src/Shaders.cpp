#include "Shaders.h"



Shaders::Shaders()
{
}


Shaders::~Shaders()
{
}

const GLchar* vertexShaderSrc =
"#version 330 core\n"
"layout (location=0) in vec4 pos;"
"void main()"
"{"
"	gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);"
"}";

const GLchar* fragmentShaderSrc =
"#version 330 core\n"
"out vec4 frag_color;"
"void main()"
"{"
"	frag_color = vec4(.35f, .96f, .3f, 1.0);"
"}";


void Shaders::turnOn()
{
	/////////////SHADERSSSSS//////////////
	GLint result;
	GLchar infoLog[512];

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);//type of shhader
	glShaderSource(vs, 1, &vertexShaderSrc, NULL);//copy paste shader code
												  //into openGL
	glCompileShader(vs); //Compile the shader

	glGetShaderiv(vs, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(vs, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Fragment failed to compile." << infoLog << endl;
	}

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragmentShaderSrc, NULL);
	glCompileShader(fs);
	if (!result)
	{
		glGetShaderInfoLog(fs, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Fragment failed to compile." << infoLog << endl;
	}


	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vs);
	glAttachShader(shaderProgram, fs);
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shaderProgram, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Shader Program Linker failure" << infoLog << endl;
	}

	glDeleteShader(vs);
	glDeleteShader(fs);
}