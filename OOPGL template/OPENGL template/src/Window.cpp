#include "Window.h"


Window::Window()
{
}


Window::~Window()
{
}

void onKeyPressed(GLFWwindow* window, int key, int scanCode, int action, int mode);
void onCursorUpdated(GLFWwindow * window, double x, double y);
void onClick(GLFWwindow * window, int button, int action, int mode);


int Window::Initialize()
{
	if (!glfwInit())
	{
		cerr << "GLFW failed to initialize" << endl;
		return -1;
	}

	//sets opengl version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//Set openGl to modern Open GL
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//set forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	SetScreen(false);

	if (window == NULL)
	{
		cerr << "GLFWwindow failed to initialize" << endl;
		glfwTerminate();
		return -1;
	}

	

	//set window as current context
	glfwMakeContextCurrent(window);

	//input callback
	glfwSetKeyCallback(window, onKeyPressed);
	glfwSetMouseButtonCallback(window, onClick);
	glfwSetCursorPosCallback(window, onCursorUpdated);

	glewExperimental = GL_TRUE; //IMPORTANT DONT FORGET
	if (glewInit() != GLEW_OK)
	{
		cerr << "Glew initialization failed" << endl;
	}
}

void onKeyPressed(GLFWwindow* window, int key, int scanCode, int action, int mode)
{
	//check if esc key was pressed
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		//close the window
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

}

void onCursorUpdated(GLFWwindow * window, double x, double y)
{
	cout << "Cursor : (" << x << " , " << y << ")" << endl;
}

void onClick(GLFWwindow * window, int button, int action, int mode)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		cout << "Click Click" << endl;
	}
}

void Window::SetWHeight(float Height)
{
	WindowWHeight = Height;
}

void Window::SetWLength(float Length)
{
	WindowWLength = Length;
}

void Window::SetScreen(bool Resize)
{
	isFullScreen = Resize;

	if (isFullScreen)
	{
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* vidMode = glfwGetVideoMode(monitor);

		if (vidMode != NULL)
		{
			window = glfwCreateWindow(vidMode->width, vidMode->height, "dirty dan", NULL, NULL);
		}
	}

	else
	{
		window = glfwCreateWindow(WindowWLength, WindowWHeight, "dirty dan", NULL, NULL);
	}
}

