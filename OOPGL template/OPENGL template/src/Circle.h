#pragma once
#include "Mesh.h"

class Circle : protected Mesh
{
public:
	Circle();
	Circle(int stacks, int slices, float radius, Color color);
	~Circle();
	void Draw();
private:
	Color mColor;
	int mStacks;
	int mSlices;
	int mRadius;
};

