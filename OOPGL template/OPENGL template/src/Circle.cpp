#include "Circle.h"

Circle::Circle()
{
}


Circle::Circle(int stacks, int slices, float radius, Color color)
{
	this->mStacks = stacks;
	this->mSlices = slices;
	this->mRadius = radius;
	this->mColor = color;


	int total = mStacks* mSlices;
	Vertex* sphereVertices =  new Vertex[total];
	int count = 0;

	for (int curStack = 0; curStack < mStacks; curStack++)
	{
		for (int curSlice = 0; curSlice < mSlices; curSlice++)
		{
			float y = -cos(3.14f * curStack / mStacks);

			float XRadius = sqrt(1 - pow(y, 2));

			float x = XRadius * sin(2.0 * 3.14f * curSlice / mSlices);
			float z = XRadius * cos(2.0 * 3.14f * curSlice / mSlices);

			sphereVertices[count] = Vertex(Vector3(x * radius, y * radius, z * radius));
			cout << x << ", " << y << ", " << z << endl;
			count++; 
		}
	}

	//int vertexCount = sizeof(sphereVertices);


	// Store all vertex information in seperate arrays
	GLfloat *positions = new GLfloat[total * 3];
	unsigned int vectorCounter = 0;
	for (unsigned int i = 0; i < total; i++) {
		positions[vectorCounter] = sphereVertices[i].position.x;
		positions[vectorCounter + 1] = sphereVertices[i].position.y;
		positions[vectorCounter + 2] = sphereVertices[i].position.z;
		vectorCounter = 3 * (i + 1);
	}

	glGenBuffers(1, &mVbo);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * total * 3, positions, GL_STATIC_DRAW);

	GLfloat *colors = new GLfloat[total * 3];
	unsigned int colorCounter = 0;
	for (unsigned int i = 0; i < total; i++) {
		colors[colorCounter] = color.r;//sphereVertices[i].color.r;
		colors[colorCounter + 1] = color.g;//sphereVertices[i].color.g;
		colors[colorCounter + 2] = color.b;//sphereVertices[i].color.b;
		colorCounter = 3 * (i + 1);
	}

	glGenBuffers(1, &mVboColor);
	glBindBuffer(GL_ARRAY_BUFFER, mVboColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * total * 3, colors, GL_STATIC_DRAW);


	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);

	// Set the active buffer to position buffer
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	// Attributes for position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	// Set the active buffer to color buffer
	glBindBuffer(GL_ARRAY_BUFFER, mVboColor);
	// Attributes for color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);


	delete[] positions;
	delete[] colors;
}

void Circle::Draw()
{
	glBindVertexArray(mVao);
	glDrawArrays(GL_TRIANGLE_FAN,0 , 1000 * 3);
	glBindVertexArray(0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

Circle::~Circle()
{

}
