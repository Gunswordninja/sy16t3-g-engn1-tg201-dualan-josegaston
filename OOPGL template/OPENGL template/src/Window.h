#pragma once

#ifndef WINDOW_H
#define WINDOW_H
#include "HeaderFiles/EngineConstruction.h"
using namespace std;

class Window
{
public:
	Window();
	~Window();
	void SetWHeight(float Height);
	void SetWLength(float Length);
	void SetScreen(bool Resize);
	int Initialize();
	GLFWwindow* window = NULL;
private:
	int WindowWHeight;
	int WindowWLength;
	string AppName;
	bool isFullScreen;	
};

#endif