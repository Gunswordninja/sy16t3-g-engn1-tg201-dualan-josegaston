#pragma once
#include "Vertex.h"

class Mesh
{
public:
	Mesh();
	Mesh(Vertex vertices[], int vertexCount);
	void Draw();

	~Mesh();

	//void Draw(GLfloat *TransVertices, int size);
protected :
	//GLfloat *vertices;
	GLuint mVbo; //vertex buffer object
	GLuint mVao; // Vertex array object
	GLuint mVboColor; //Vertex Color
	float mSize;
};

