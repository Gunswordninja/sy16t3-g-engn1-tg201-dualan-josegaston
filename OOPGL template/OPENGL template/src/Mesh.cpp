#include "Mesh.h"


Mesh::Mesh()
{

}

Mesh:: Mesh(Vertex vertices[], int vertexCount)
{
	if (vertexCount == 0) return;

	mSize = vertexCount;

	// Store all vertex information in seperate arrays
	GLfloat *positions = new GLfloat[vertexCount * 3];
	unsigned int vectorCounter = 0;
	for (unsigned int i = 0; i < vertexCount; i++) {
		positions[vectorCounter] = vertices[i].position.x;
		positions[vectorCounter + 1] = vertices[i].position.y;
		positions[vectorCounter + 2] = vertices[i].position.z;
		vectorCounter = 3 * (i + 1);
	}

	glGenBuffers(1, &mVbo);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexCount * 3, positions, GL_STATIC_DRAW);

	GLfloat *colors = new GLfloat[vertexCount * 3];
	unsigned int colorCounter = 0;
	for (unsigned int i = 0; i < vertexCount; i++) {
		colors[colorCounter] = vertices[i].color.r;
		colors[colorCounter + 1] = vertices[i].color.g;
		colors[colorCounter + 2] = vertices[i].color.b;
		colorCounter = 3 * (i + 1);
	}

	glGenBuffers(1, &mVboColor);
	glBindBuffer(GL_ARRAY_BUFFER, mVboColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexCount * 3, colors, GL_STATIC_DRAW);


	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);

	// Set the active buffer to position buffer
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	// Attributes for position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	// Set the active buffer to color buffer
	glBindBuffer(GL_ARRAY_BUFFER, mVboColor);
	// Attributes for color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	delete[] positions;
	delete[] colors;

}


Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &mVao);
	glDeleteBuffers(1, &mVbo);
}

void Mesh::Draw()
{

}

/*void Mesh::Draw(GLfloat *TransVertices, int verticeSize)
{
	vertices = TransVertices;
	
}*/
