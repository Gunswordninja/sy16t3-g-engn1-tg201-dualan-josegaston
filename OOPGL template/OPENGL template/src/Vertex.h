#pragma once

#include "Color.h"
#include "Vector3.h"

class Vertex
{
public:
	Vertex();
	Vertex(Vector3 Position);
	Vertex(Vector3 Position, Color Color);
	~Vertex();

	Vector3 position;
	Color color;
};

