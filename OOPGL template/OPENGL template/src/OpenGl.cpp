
#include "Window.h"
#include "Shaders.h"
#include "Mesh.h"
#include "Circle.h"

using namespace std;

//Simulation
void gameUpdate(GLFWwindow* window, double dt);
void renderUpdate(GLFWwindow* window, double dt, GLuint shaderProgam, GLuint vao);
void physicsUpdate(GLFWwindow* window, double dt);

void showFPS(GLFWwindow* window, double dt);

int main()
{
	
	Window* newScreen = new Window();
	newScreen->SetWHeight(800);
	newScreen->SetWLength(800);
	newScreen->Initialize();

	Vertex vertices[] = {
		Vertex(Vector3(0.0f, 0.5f, 0.0f), Color::Red),
		Vertex(Vector3(0.5f, -0.5f, 0.0f), Color::Green),
		Vertex(Vector3(-0.5f, -0.5f, 0.0f), Color::Blue)
	};
	
	//Mesh* newMesh = new Mesh(vertices, sizeof(vertices));
	//newMesh->Draw(vertices, sizeof(vertices));
	
	Circle* newCircle = new Circle(25, 25, .5f, Color::Red);


	Shaders* newShader = new Shaders(); 
	newShader->turnOn();


	glClearColor(.23f, .38f, .47f, 1.0f);

	double prevTime = 0;

	

	while (!glfwWindowShouldClose(newScreen->window))
	{
		//compute Delta time
		double currentTime = glfwGetTime();
		double deltaTime = currentTime - prevTime;
		prevTime = currentTime;

		//1. Physics

		//2. Input
		glfwPollEvents();

		//3. Game Loop
		//gameUpdate(newScreen-> window, deltaTime);

		//4. Render loop
		
		glClear(GL_COLOR_BUFFER_BIT);
		
		//renderUpdate(newScreen -> window, deltaTime, newShader->shaderProgram, newMesh->vao);
		newCircle->Draw();
		glfwSwapBuffers(newScreen->window);
		// Main application, game loop

	}

	cin.sync();
	cin.ignore();
	glfwTerminate();
	system("pause");
	return 0;
}



void gameUpdate(GLFWwindow * window, double dt)
{
	cout << dt <<  endl;
}

void renderUpdate(GLFWwindow * window, double dt, GLuint shaderProgam, GLuint vao)
{
	glUseProgram(shaderProgam);
	

	
	/*glUseProgram(shaderProgam);
	glBindVertexArray(vao);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
	glBindVertexArray(0);*/
}

void physicsUpdate(GLFWwindow * window, double dt)
{
}

void showFPS(GLFWwindow * window, double dt)
{
	//double
}
