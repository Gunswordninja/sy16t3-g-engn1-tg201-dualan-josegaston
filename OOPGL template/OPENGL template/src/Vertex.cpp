#include "Vertex.h"



Vertex::Vertex()
{
}

Vertex::Vertex(Vector3 Position)
{
	position = Position;
	color = Color::White;
}

Vertex::Vertex(Vector3 Position, Color Color)
{
	this -> position = Position;
	this -> color = Color;
}

Vertex::~Vertex()
{
}
