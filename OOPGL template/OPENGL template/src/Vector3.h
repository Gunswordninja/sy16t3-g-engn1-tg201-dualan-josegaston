#pragma once
#include "HeaderFiles/EngineConstruction.h"

struct Vector3
{
	Vector3();
	Vector3(GLfloat x, GLfloat y, GLfloat z);

	GLfloat x;
	GLfloat y;
	GLfloat z;
};