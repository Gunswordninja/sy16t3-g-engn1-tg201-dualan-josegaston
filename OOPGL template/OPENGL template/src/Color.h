#pragma once
#include "HeaderFiles/EngineConstruction.h"

class Color
{
public:
	Color();
	Color(GLfloat r , GLfloat b, GLfloat g);

	GLfloat r;
	GLfloat b;
	GLfloat g;
	~Color();


	static const Color Black;
	static const Color White;
	static const Color Red;
	static const Color Green;
	static const Color Blue;
	static const Color Yellow;
	static const Color Cyan;
	static const Color Magenta;
};

