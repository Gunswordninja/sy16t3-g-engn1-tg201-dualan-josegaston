#pragma once
#include "HeaderFiles/EngineConstruction.h"
class Shaders
{
public:
	Shaders();
	~Shaders();
	void turnOn();

private:
	GLint shaderProgram = 0;
	
};

