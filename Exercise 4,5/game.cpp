// game.cpp

#include "Map.h"
#include "Player.h"
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
using namespace std;

int main()
{
	srand( time(0) );

	Map gameMap;

	Player mainPlayer;

	mainPlayer.createClass();

	// Begin adventure.
	bool done = false;
	while( !done )
	{
		// Each loop cycly we output the player position and 
		// a selection menu.

		gameMap.printPlayerPos();
	
		int selection = 1;
		cout << "1) Move, 2) Rest, 3) View Stats, 4) Quit: ";
		cin >> selection;

		std::vector<Monster*> Monsters;

		switch( selection )
		{
		case 1:
			gameMap.movePlayer();

			// Check for a random encounter.  This function
			// returns a null pointer if no monsters are
			// encountered.
			
			for (int i = 0; i < 3 ; i++)
			{
				Monster* add = gameMap.checkRandomEncounter();
				if (add != NULL)
				{
					Monsters.push_back(add);
				}
				
			}
			

			// 'monster' not null, run combat simulation.
			if( Monsters.size() != 0 )
			{

				cout << "Prepare for battle!" << endl;
				// Loop until a 'break' statement.
				while( true )
				{
					// Display hitpoints.
					mainPlayer.displayHitPoints();
					
					
					for (int i = 0; i < Monsters.size() ; i++)
					{
						cout << i + 1 << ". ";
						Monsters[i]->displayHitPoints();
					}
					
						cout << endl;
						cout << "Enter the number of which monster you would like to attack" << endl;
						int attackChoice = 0;
					
						while (true)
						{
							cin >> attackChoice;
							
							if (attackChoice <= Monsters.size())
							{
								break;1
									1
							}
						}
						

						// Player's turn to attack first.
						bool runAway = mainPlayer.attack(*Monsters[attackChoice-1]);

						if( runAway )
							break;

						Monster* toDelete = NULL;
						
						for (int i = 0; i < Monsters.size(); i++)
						{
							if (Monsters[i]->isDead())
							{
								mainPlayer.victory(Monsters[i]->getXPReward());
								toDelete = Monsters[i];
								Monsters.erase(Monsters.begin()+i);
								break;
							}

							Monsters[i]->attack(mainPlayer);
						}
						
						delete toDelete;


						if( mainPlayer.isDead() )
						{
							mainPlayer.gameover();
							done = true;
							break;
						}

						if (Monsters.size() == 0)
						{
							mainPlayer.levelUp();
							break;
						}
						
				}

				// The pointer to a monster returned from
				// checkRandomEncounter was allocated with
				// 'new', so we must delete it to avoid
				// memory leaks.
			}

			break;
		case 2:
			mainPlayer.rest();
			break;
		case 3:
			mainPlayer.viewStats();
			break;
		case 4:
			done = true;
			break;
		}
	}	
}