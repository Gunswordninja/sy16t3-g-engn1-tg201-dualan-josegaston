// Monster.h

#ifndef MONSTER_H
#define MONSTER_H

#include "Unit.h"
#include <string>

// "Forward class declaration" so that we can use the Player
// class without having defined it yet.  This idea is 
// similar to a function declaration.

class Player;

class Unit;

class Monster : public Unit
{
public:
	Monster(const std::string& name, int hp, int acc,
		int xpReward, int armor, const std::string& weaponName,
		int lowDamage, int highDamage);

	int         getXPReward();
	
	std::string getName();

	void attack(Player& player);

private:
	int         mExpReward;
};

#endif //MONSTER_H