#pragma once

#ifndef UNIT_H
#define UNIT_H

#include "Weapon.h"

class Unit
{
protected: 
	// inherited data members
	std::string mName;
	int         mHitPoints;
	int         mAccuracy;
	int         mArmor;
	Weapon      mWeapon;
	
public:
	//Methods
	bool isDead();
	int  getArmor();
	void displayHitPoints();	
	void takeDamage(int damage);
	Unit();
	~Unit();
};

#endif //UNIT_H
