// Map.cpp

#include "Map.h"
#include <iostream>
#include "Random.h"
using namespace std;

Map::Map()
{
	// Player starts at origin (0, 0)
	mPlayerXPos = 0;
	mPlayerYPos = 0;
}

int  Map::getPlayerXPos()
{
	return mPlayerXPos;
}

int  Map::getPlayerYPos()
{
	return mPlayerYPos;
}

void Map::movePlayer()
{
	int selection = 1;
	cout << "1) North, 2) East, 3) South, 4) West: ";
	cin >> selection;

	// Update coordinates based on selection.
	switch( selection )
	{
	case 1: // North
		mPlayerYPos++;
		break;
	case 2: // East
		mPlayerXPos++;
		break;
	case 3: // South
		mPlayerYPos--;
		break;
	default: // West
		mPlayerXPos--;
		break;
	}
	cout << endl;
}

int Map::getNumOfMonsters()
{
	return NumOfMonsters;
}

Monster* Map::checkRandomEncounter()
{
	int roll = Random(35, 50);

	Monster* Spawn;
	cout << "Monster Roll : " << roll << endl;


	if (roll <= 35)
	{
		// No encounter, return a null pointer.
		return NULL;
	}

	if (roll >= 36 && roll <= 50)
	{

			 if (roll >= 36 && roll <= 40)
			{
				Spawn = new Monster("Orc", 10, 8, 200, 1,
					"Short Sword", 2, 7);

				cout << "You encountered an Orc!" << endl;
				cout << endl;
			}
			else if (roll >= 41 && roll <= 45)
			{
				Spawn = new Monster("Goblin", 6, 6, 100, 0,
					"Dagger", 1, 5);

				cout << "You encountered a Goblin!" << endl;
				cout << endl;
			}
			
			else if (roll >= 46 && roll <= 49)
			{
				Spawn = new Monster("Ogre", 20, 12, 500, 2,
					"Club", 3, 8);

				cout << "You encountered an Ogre!" << endl;
				cout << endl;
			}
			
			else if (roll == 50)
			{
				Spawn = new Monster("Orc Lord", 25, 15, 2000, 5,
					"Two Handed Sword", 5, 20);

				cout << "You encountered an Orc Lord!!!" << endl;;
				cout << endl;
			}

		return Spawn;
	}
}

void Map::printPlayerPos()
{
	cout << "Player Position = (" << mPlayerXPos << ", " 
		<< mPlayerYPos << ")" << endl << endl;
}