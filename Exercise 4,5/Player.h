// Player.h

#ifndef PLAYER_H
#define PLAYER_H

#include "Unit.h"
#include "Monster.h"
#include <string>

class Player : public Unit
{
public:
	// Constructor.
	Player();

	// Methods

	void createClass();
	bool attack(Monster& monster);
	void levelUp();
	void rest();
	void viewStats();
	void victory(int xp);
	void gameover();
 
private:
	// Data members.
	std::string mClassName;
	int         mMaxHitPoints;
	int         mExpPoints;
	int         mNextLevelExp;
	int         mLevel;
};

#endif //PLAYER_H