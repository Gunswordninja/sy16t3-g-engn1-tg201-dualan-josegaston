#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

bool Unit::isDead()
{
	return mHitPoints <= 0;
}

void Unit::displayHitPoints()
{
	cout << mName << "'s hitpoints = " << mHitPoints << endl;
}

int Unit::getArmor()
{
	return mArmor;
}

void Unit::takeDamage(int damage)
{
	mHitPoints -= damage;
}

Unit::Unit()
{
}


Unit::~Unit()
{
}
