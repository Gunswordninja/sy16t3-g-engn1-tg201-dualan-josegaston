#include <iostream>
#include <math.h>
#include <cmath>
#include <string>

using namespace std;

float Result(float points[4])//int x, int y, int x2, int y2)
{
	int Calculation = pow((points[0] - points[1]), 2) + pow((points[2] - points[3]), 2);
	return sqrt(Calculation);
}

int main()
{
	float points[4];
	string xypointstring[4] = { "x1", "y1", "x2", "y2" };

	int Maxdistance = 6000;
	int MaxStunSec = 5;
	int MaxStunDistance = 600;
	bool Miss = false;

	cout << "Enter max distance " << endl;
	cin >> Maxdistance;
	cout << "Enter maximum stun seconds" << endl;
	cin >> MaxStunSec;
	cout << "Enter distance to stun rate" << endl;
	cin >> MaxStunDistance;

	for (int i = 0; i < 4; i++)
	{
		cout << "Enter point " << xypointstring[i] << endl;
		cin >> points[i];
	}

	float ConvertedDistance = Result(points) * 1000;
	cout << "Distance : " << ConvertedDistance << endl;

	if (ConvertedDistance > Maxdistance)
	{
		Miss = true;
	}

	float stunSecondsCalculation = Result(points) * 1000 / MaxStunDistance;

	if (stunSecondsCalculation > MaxStunSec)
	{
		stunSecondsCalculation = 5;
	}
	
	cout << "Stun Seconds : " << stunSecondsCalculation << endl;

	if (Miss) cout << " You missed you scrub!" << endl;
	if (!Miss) cout << "You hit the target!" << endl;

	system("pause");
}

