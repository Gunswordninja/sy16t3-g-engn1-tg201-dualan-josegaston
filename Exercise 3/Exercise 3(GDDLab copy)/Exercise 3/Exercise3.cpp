#include <stdio.h>      
#include <stdlib.h>     
#include <conio.h>
#include <time.h>
#include <iterator>
#include <iostream>
#include <array>
#include <algorithm>
#include <vector>
#include <list>

void addToArray(int** numberArray, int &size, int arrayStart);
void deleteLast(int** numberArray, int &size);
void deleteFirst(int** numberArray, int size, int &arrayStart);
void intputvalues(int* numberArray, int size);
void printvalues(int* numberArray, int arraySize, int arrayStart);
void userOptions();

void addToArray(int** numberArray, int &size, int arrayStart)
{
	size += 1;
	std::cout << "Input Array number " << size-arrayStart << std::endl;
	int* value = new int;
	std::cin >> *value;
	numberArray[size-1] = &*value;
}

void deleteFirst(int** numberArray, int size, int &arrayStart)
{
	delete[] numberArray[0];
	arrayStart += 1;
	numberArray[0] = NULL;
	//size -= 1;
	//return (numberArray); //this works
}

void deleteLast(int** numberArray, int &size)
{
	std::string poop;
	delete[] numberArray[size - 1];
	numberArray[size - 1] = NULL;
	size -= 1;
	//return (deleteArray + *size);

}

void inputvalues(int** numberArray, int arraySize)
{
	for (int i = 0; i < arraySize; i++)
	{
		std::cout << "Input element number " << i + 1 << std::endl;
		int* value = new int;
		std::cin >> *value;
		numberArray[i] = &*value;
	}
}

void printvalues(int** numberArray, int arraySize, int arrayStart)
{
	std::cout << "Array Numbers : " << std::endl << std::endl; 
	std::cout << "size: "<< arraySize - arrayStart << std::endl;

	for (int i = arrayStart; i < arraySize; i++)
	{
		std::cout << **(numberArray + i) << std::endl;	
	}
	
}


void userOptions(int **numberArray, int &arraySize, int &arrayStart, bool &loop)
{
	int i;

	std::cout << std::endl << "Press 1 to add a new array element" << std::endl;
	std::cout << "Press 2 to delete a first array element" << std::endl;
	std::cout << "Press 3 to delete a last array element" << std::endl;
	std::cout << "Press 4 to exit" << std::endl;
	std::cin >> i;

	switch (i)
	{
	case 1:
		addToArray(numberArray, arraySize, arrayStart);
		break;
	case 2:
		deleteFirst(numberArray, arraySize, arrayStart);
		break;
	case 3:
		deleteLast(numberArray, arraySize);
		break;
	case 4:
		loop = false;
		break;
	default :
		break;
	}
}

int main()
{
	//Ask user for the size of the array
	
	int* arraySize = new int;
	int* arrayStart = new int;
	*arrayStart = 0;
	*arraySize = 0;
	int** newArray;
	std::cout << "Input size of array" << std::endl;
	std::cin >> *arraySize;
	newArray =  new int*[*arraySize];
	std::cout << "Size of the array is " << *arraySize << std::endl << std::endl;
	
	//Ask the user to input the values of the array
	inputvalues(newArray, *arraySize);
    printvalues(newArray, *arraySize,*arrayStart);
	
	//Ask for extra functions to add or delete
	bool loop = true;
	while (loop == true)
	{
		userOptions(newArray, *arraySize, *arrayStart, loop);
		system("CLS");
		printvalues(newArray, *arraySize, *arrayStart);
	}

	return 0;
}